import os
import random
import string

from flask import Flask, request, render_template
from werkzeug.utils import secure_filename

from functions import projection_functions
from functions.file_convert import FileConverter
from main import Main

UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/uploads/'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'doc', 'docx', 'odt'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
path = os.path.dirname(os.path.abspath(__file__))
print(path)


@app.route('/resume')
def show_resume(name1):
    try:
        name, email, experience, number, location, study_years, \
        education, work, site_name, site_link, personal, \
        summary, cluster, skills, summary_pub, \
        d_pub, summary_proj, date_ret_low, date_ret_high = Main(
            name1.split('.')[0] + '.txt').main()
        f_name, l_name = projection_functions.Functions.name_split(name)
        l_w_year, h_w_year, work = projection_functions.Functions.find_work(list(work))
        l_year, h_year, edu_list = projection_functions.Functions.find_edu(education)
        return render_template('index.html', fname=f_name, lname=l_name, email=email, path=path,
                               experience=experience, number=number, location=location,
                               study_years=study_years, l_w_year=l_w_year, work=work,
                               h_w_year=h_w_year, l_year=l_year, h_year=h_year,
                               edu_list=edu_list, site_name=site_name, site_link=site_link
                               , personal_link=personal, summary=summary,
                               cluster=cluster, skills=skills, summary_pub=summary_pub,
                               d_pub=d_pub, summary_proj=summary_proj,
                               date_ret_low=date_ret_low, date_ret_high=date_ret_high)
    except Exception as exception:
        print(exception)
        return render_template('index.html', name=str(exception), path=os.listdir(os.getcwd()))


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route("/", methods=['GET', 'POST'])
def index():
    message = ''
    if request.method == 'POST':
        file = request.files['file[]']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename).split('.')
            while True:
                name = ''.join(
                    random.SystemRandom().choice(
                        string.ascii_uppercase + string.digits + string.ascii_lowercase)
                    for _ in range(10)) + '.' + filename[1]
                if filename not in os.listdir(app.config['UPLOAD_FOLDER'], ):
                    break
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], name))
            FileConverter(path + '/uploads/', path + '/resume_samples', name)
            return show_resume(name)
        else:
            message = '* Error: File not supported'
    return render_template('upload.html', message=message)


if __name__ == '__main__':
    app.run(debug=True)
