import os

from functions import *


class Main(object):
    def __init__(self, name):
        self.name = name
        self.path = os.path.dirname(os.path.abspath(__file__)) + '/resume_samples/'

        self.main()

    def main(self):
        f = self.path + self.name
        print('path: {}'.format(f))
        resume = open(f, 'r', encoding="utf8", errors='ignore').read()
        name, email, experience, site_name, site_link, personal, summary = \
            basic_info.FindBasics(resume, os.path.dirname(os.path.abspath(__file__))).main()
        number, location = phone_loc.PhoneLoc(str(f), resume).main()
        study_years, education, work, summary_pub, \
        d, summary_proj, date_ret_low, date_ret_high = feature.Feature(resume).main()
        # cluster, skills = clusters.Clusters(resume).find_cluster()
        cluster = ['human_resource']
        skills = ['java', 'python']
        print(location)
        return name, email, experience, number, location, \
               study_years, education, work, site_name, site_link, personal, \
               summary, cluster, skills, summary_pub, \
               d, summary_proj, date_ret_low, date_ret_high
