import collections
import math

import numpy as np
import pandas as pd
from pymongo import MongoClient

from functions import process_text


class Clusters(object):
    def __init__(self, resume):
        self.Resume = resume
        self.Db = MongoClient().keywordsDb
        self.Db2 = MongoClient().clustersDb
        self.Text = process_text.Text(resume)

    @staticmethod
    def find_skills(bow, db):
        try:
            skills = []
            for i in bow:
                cursor = db.keywords.find({'tag_a': i})
                if cursor.count() > 0:
                    skills.append(i)
            list_1 = set(skills)
            return list_1, skills
        except Exception as exception:
            print(exception)

    @staticmethod
    def find_stream(list_1, db):
        try:
            k = 0
            df = pd.DataFrame(index=list_1, columns=list_1)
            for i in list_1:
                for j in list_1:
                    k += 1
                    cursor = db.keywords.find({"tag_a": {'$regex': i},
                                               "tag_b": {'$regex': j}})
                    if cursor.count() > 0:
                        for l in cursor:
                            if l == '':
                                pass
                            df[i][j] = l['eu_dist']
                    else:
                        df[i][j] = 1024
            l = []
            l.append([""] + list(df.columns.values))
            for x, item in enumerate(df.index.values):
                l.append([item] + list(df.values[x]))
            list_2 = [i for i in df[df > .01].min()]
            k = 0
            json = {}
            for i in list_2:
                json[i] = []

            for i in df:
                for j in df.columns.values:
                    if list_2[k] == df[i][j]:
                        json[df[i][j]].append([i, j])
                k += 1
            p = math.ceil(0.2 * len(json))
            od = collections.OrderedDict(sorted(json.items()))
            kk = 0
            exp = {}
            for key in od:
                if kk < p:
                    for i in od[key]:
                        cursor = db.keywords.find({'tag_a': {'$regex': i[0]},
                                                   'tag_b': {'$regex': i[1]}})
                        for c in cursor:
                            if c['eu_dist'] not in exp:
                                exp.update({c['eu_dist']: []})
                            exp[c['eu_dist']].append(c['expertise'])
                kk += 1
            kk = 0
            od2 = collections.OrderedDict(sorted(exp.items()))
            return_element = []
            for key in od2:
                if kk < 1:
                    for i in set(od2[key]):
                        return_element.append(i)
                    kk += 1
            return return_element
        except Exception as exception:
            print(exception)

    def find_cluster(self):
        try:
            print('Please wait as we process your resume...')
            list_1, skills = self.find_skills(self.Text.process_keywords(), self.Db)
            dictionary = sorted(collections.Counter(skills).items(), key=lambda x: x[1])
            num_list = []
            for i, j in dictionary:
                num_list.append(j)
            num_list = set(num_list)
            num_list = np.array(list(map(int, num_list)))
            num_list = num_list[int(np.median(num_list)):]
            if len(num_list) >= 4:
                num_list = [x for x in num_list if x > int(np.median(num_list))]
            list_f = [i for i, j in dictionary if j in num_list]
            first_set = self.find_stream(list_f, self.Db)
            second_set = self.find_stream(list_1, self.Db)
            cluster_select = {}
            for cl in first_set:
                for s_cl in second_set:
                    cursor_clust = self.Db2.clustersDb.find({'clust_a': cl, 'clust_b': s_cl})
                    for c in cursor_clust:
                        if int(c['similarity']) not in cluster_select:
                            cluster_select.update({int(c['similarity']): []})
                            cluster_select[int(c['similarity'])].append([cl, s_cl])

            cluster_select = collections.OrderedDict(sorted(cluster_select.items()))
            probable = []
            cluster_count = 0
            for key in cluster_select:
                if cluster_count == 0:
                    for i, j in cluster_select[key]:
                        if i != j:
                            cluster_count += 1
                        probable.append(i)
                        probable.append(j)
            return list(set(probable)), list(list_1)
        except Exception as exception:
            print(exception)