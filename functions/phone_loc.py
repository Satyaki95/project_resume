import re

import phonenumbers
from phonenumbers import geocoder


class PhoneLoc(object):
    def __init__(self, path, string):
        self.Path = path
        self.String = string

    def main(self):
        number = list(set(self.extract()))
        location = self.location()
        print(number)
        return number, location

    def extract(self):
        try:
            r = re.compile(r'(\d{3}[-\.\s]??\d{3}'
                           r'[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}'
                           r'[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')
            phone_numbers = r.findall(self.String)
            extracted = [re.sub(r'\D', '', number) for number in phone_numbers if len(number) > 8]
            with open(self.Path) as f:
                content = f.readlines()
            number = []
            for i in content:
                num = []
                for j in i:
                    if j.isdigit():
                        num.append(j)
                for k in extracted:
                    if str(k) in str(''.join(num)):
                        number.append(''.join(num))
            formatted_num = []
            for k in extracted:
                for i in number:
                    n = str(k[0]) + str(k[1]) + str(k[2])
                    if k[0] == i[0] and k[1] == i[1] and k[2] == i[2]:
                        formatted_num.append(i)
                    elif n in i:
                        if i[i.index(n[0])] == n[0] \
                                and i[i.index(n[0]) + 1] == n[1] \
                                and i[i.index(n[0]) + 2] == n[2]:
                            formatted_num.append(i[i.index(n[0]):])
            new_num = list(set(formatted_num + number))
            new_num = [i for i in new_num if len(i) < 16]
            return new_num
        except Exception as exception:
            print(exception)

    def location(self):
        try:
            num = list(set(self.extract()))
            num = str("+" + str(num[0]))
            ch_number = phonenumbers.parse(num, "CH")
            if len(repr(geocoder.description_for_number(ch_number, "en"))) == 2:
                return 'Unknown'
            else:
                return repr(geocoder.description_for_number(ch_number, "en"))
        except Exception as exception:
            print(exception)

# number, location = \
#     PhoneLoc(str('../resume_samples/rhcmSqO62k.txt'),
#                                       open('../resume_samples/rhcmSqO62k.txt', 'r').read()).main()
# print(number, location)