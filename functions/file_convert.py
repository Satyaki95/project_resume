import re
from subprocess import Popen, PIPE

import magic
from docx import Document
from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBox, LTTextLine
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfparser import PDFParser, PDFDocument
from pyth.plugins.plaintext.writer import PlaintextWriter
from pyth.plugins.rtf15.reader import Rtf15Reader


class FileConverter(object):
    def __init__(self, path, output, name):
        self.output = output
        self.path = path + name
        self.name = name
        self.main()

    def main(self):
        with open(self.output + '/' + self.name.split('.')[0] + '.txt', 'w') as out:
            cont = self.file_to_txt()
            out.write(cont['content'])
            out.close()

    def get_mimetype_from_file(self):
        f = magic.Magic(mime=True)
        return f.from_file(self.path)

    def pdf_to_txt(self):
        fp = open(self.path, 'rb')
        parser = PDFParser(fp)
        doc = PDFDocument()
        parser.set_document(doc)
        doc.set_parser(parser)
        doc.initialize('')
        rsrcmgr = PDFResourceManager()
        laparams = LAParams()
        device = PDFPageAggregator(rsrcmgr, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        # Process each page contained in the document.
        list = []
        for page in doc.get_pages():
            interpreter.process_page(page)
            layout = device.get_result()
            for lt_obj in layout:
                if isinstance(lt_obj, LTTextBox) or isinstance(lt_obj, LTTextLine):
                    list.append(lt_obj.get_text())
        return '\n'.join(list)

    def rtf_to_txt(self):
        doc = Rtf15Reader.read(open(self.path))
        print('here')
        return PlaintextWriter.write(doc).getvalue()

    def doc_to_txt(self):
        cmd = ['antiword', self.path]
        p = Popen(cmd, stdout=PIPE)
        stdout, stderr = p.communicate()
        return stdout.decode('ascii', 'ignore')

    def docx_to_txt(self):
        document = Document(self.path)
        paratextlist = document.paragraphs
        newparatextlist = [i.text for i in paratextlist]
        newparatextlist = [i.replace('\t', '') for i in newparatextlist]
        newparatextlist = [i.strip() for i in newparatextlist if i]
        return '\n'.join(newparatextlist)

    def odt_to_txt(self):
        cmd = ['odt2txt', self.path]
        p = Popen(cmd, stdout=PIPE)
        stdout, stderr = p.communicate()
        # print stderr
        # print
        txt_list = stdout.decode('ascii', 'ignore').split('\n')
        txt_list = [i for i in txt_list if i]
        return '\n'.join(txt_list)

    def clean_txt(self, text):
        # return text
        # text = unicode(text, 'utf-8')
        # return re.sub(r'[ ;\'\n.\.,\xc2\xa0\xe2\x80\x99\xe2\x80\x98\xad\x8b]', ' ', text)
        text = text.lower()
        # return re.sub(r'[^\x00-\x7F]+',' ', text)
        text = re.sub('[,\.\(\)\s+]', ' ', text)
        text = re.sub('\s+', ' ', text)
        return re.sub(r'[^\x00-\x7F]+', ' ', text)

    def txt_to_txt(self):
        fin = open(self.path, 'r')
        data = fin.read()
        return data

    def file_to_txt(self):
        options = {
            'application/pdf': self.pdf_to_txt,
            'application/vnd.oasis.opendocument.text': self.odt_to_txt,
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document': self.docx_to_txt,
            'text/plain': self.txt_to_txt,
            'application/msword': self.doc_to_txt,
            'text/rtf': self.rtf_to_txt,
            'text/x-pascal': self.txt_to_txt
        }

        mtype = self.get_mimetype_from_file()
        print(mtype)
        # print mtype
        # print cleaned_txt
        if mtype in options.keys():
            content = options[mtype]()
            return {
                'success': True,
                'content': options[mtype](),
                'mimetype': mtype
            }
        else:
            return {
                'success': False,
                'message': 'no converter found',
                'content': ''
            }

# FileConverter('/home/satyaki/workspace_venturesity/python/project_resume/uploads/',
#                           '/home/satyaki/workspace_venturesity/python/project_resume/resume_samples',
#                           'hg8IbHFCdo.odt')
