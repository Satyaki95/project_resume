from __future__ import unicode_literals

import re
import string

import nltk
from nltk.corpus import stopwords
from nltk.tokenize import PunktSentenceTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer


class Feature(object):
    def __init__(self, resume):
        self.lines = resume

    def main(self):
        study_years, edu, edu2 = self.education()
        work = []
        for i in edu2:
            for j in self.work():
                vect = TfidfVectorizer(min_df=1)
                tfidf = vect.fit_transform(
                    [i, j])
                a = (tfidf * tfidf.T).A
                if a[0][1] < 0.20:
                    work.append(j)
        work = set(work)
        summary_pub, d = self.publication()
        summary_proj, date_ret_low, date_ret_high = self.resume_project()

        return study_years, edu, work, summary_pub, \
               d, summary_proj, date_ret_low, date_ret_high

    def publication(self):
        resume = self.lines
        resume = [i.lower() for i in resume.split('\n')]
        summary = []
        bool = False
        for i in resume:
            if not bool:
                if re.search('publication', i) or re.search('paper', i):
                    bool = True
            else:
                if not i:
                    if len(''.join(summary)) > 100:
                        bool = False
                elif len(''.join(summary)) > 400:
                    bool = False
                else:
                    summary.append(i)
        summary = '\n'.join(summary)
        date = list(range(1900, 2100))
        d = list(map(lambda x: re.findall(str(x), summary), date))
        d = [i for i in d if i]
        d = [j for i in d for j in i]
        return summary, d

    def resume_project(self):
        resume = self.lines
        resume = [re.sub(r'[^\x00-\x7F]+',
                         ' ', re.sub('\s+', ' ',
                                     re.sub('[,\.\(\)\s+]', ' ', i.lower())))
                  for i in resume.split('\n')]
        summary = []
        bool = False
        for i in resume:
            if not bool:
                if re.search('project', i):
                    bool = True
            else:
                if not i:
                    if len(''.join(summary)) > 100:
                        bool = False
                elif len(''.join(summary)) > 400:
                    bool = False
                else:
                    summary.append(i)
        summary = [i for i in summary if i != ' ']
        date = list(range(1900, 2100))
        date_ret_low = []
        date_ret_high = []
        for i in summary:
            d = list(map(lambda x: re.findall(str(x), i), date))
            d = [i for i in d if i]
            d = [j for i in d for j in i]
            if d:
                date_ret_low.append(sorted(d)[0])
                date_ret_high.append(sorted(d)[len(d) - 1])
            else:
                date_ret_low.append('-')
                date_ret_high.append('-')
        print(date_ret_low, date_ret_high)
        return summary, date_ret_low, date_ret_high

    def education(self):
        try:
            edu = []
            year = list(range(1900, 2100))
            for line in self.lines.split('\n'):
                l = ' '.join([l.lower() for l in line.split()])
                if 'education' in l or 'university' in l or \
                                'college' in l or 'school' in l or 'institute' in l:
                    edu.append(line)
            yr = []
            yr2 = []
            calc_yr = []
            for i in edu:
                sent = []
                clear = ' '.join([word for word in i.split()
                                  if word.lower() not in stopwords.words('english')])
                list2 = list(map(lambda x: re.findall(str(x), clear), year))
                list2 = [x for x in list2 if x != []]
                list2 = [k for k in list2]
                clear = ''.join([k for k in clear if k not in
                                 string.punctuation and not k.isdigit()]).strip()
                custom_sent_tokenizer = PunktSentenceTokenizer()
                tokenized = custom_sent_tokenizer.tokenize(clear)
                for i in tokenized:
                    words = nltk.word_tokenize(i)
                    tagged = nltk.pos_tag(words)
                    for k, j in tagged:
                        if 'NNP' in j or 'JJ' in j:
                            sent.append(k)
                clear2 = ' '.join(sent).lower()
                clear = clear.lower()
                vect = TfidfVectorizer(min_df=1)
                tfidf = vect.fit_transform(
                    [clear, clear2])
                a = (tfidf * tfidf.T).A
                if a[0][1] > 0.2:
                    if len(clear) > len(clear2):
                        yr2.append(clear)
                        if not list2:
                            yr.append(clear)
                        else:
                            yr.append([clear, list2])
                    else:
                        yr2.append(clear2)
                        if not list2:
                            yr.append(clear2)
                        else:
                            yr.append([clear2, list2])
                else:
                    yr2.append(clear)
                    yr2.append(clear2)
                    if not list2:
                        yr.append(clear)
                        yr.append(clear2)
                    else:
                        yr.append([clear, list2])
                        yr.append([clear2, list2])
                if list2:
                    for j in list2:
                        for k in j:
                            calc_yr.append(k)
            if calc_yr:
                calc = int(max(calc_yr)) - int(min(calc_yr))
            else:
                calc = ''
            l_crap = ['education', 'university', 'college', 'school', 'institute']
            yr = [y for y in yr if y not in l_crap]
            yr = [y for y in yr if y]
            yr2 = [y for y in yr2 if y]
            return calc, yr, yr2
        except Exception as exception:
            print(exception)

    def work(self):
        try:
            lines = self.lines
            lines = ''.join([l for l in lines if l not in string.punctuation]).split('\n')
            year = list(range(1900, 2100))
            final = []
            for line in range(len(lines)):
                work = []
                list1 = list(map(lambda x: re.findall(str(x), lines[line]), year))
                list1 = [x for x in list1 if x]
                if list1:
                    if line == 0:
                        work.extend((lines[line],
                                     lines[line + 1], lines[line + 2]))
                    elif line == 1:
                        work.extend((lines[line - 1],
                                     lines[line], lines[line + 1], lines[line + 2]))

                    elif line == len(lines) - 1:
                        work.extend((lines[line - 2],
                                     lines[line - 1], lines[line]))
                    elif line == len(lines) - 2:
                        work.extend((lines[line - 2],
                                     lines[line - 1], lines[line], lines[line + 1]))
                    else:
                        work.extend((lines[line - 2],
                                     lines[line - 1], lines[line],
                                     lines[line + 1], lines[line + 2]))
                    work = [i for i in work if i]
                    final.append(work)
            total = []
            for line in final:
                work = []
                for l in line:
                    tokenized = PunktSentenceTokenizer().tokenize(l)
                    for i in tokenized:
                        words = nltk.word_tokenize(i)
                        tagged = nltk.pos_tag(words)
                        for i, j in tagged:
                            if re.search('NN', j):
                                work.append(i)
                total.append(' '.join(work))
            total = [i.strip().lower() for i in total if i]
            return total
        except Exception as exception:
            print(exception)


# print(Feature(open('/home/satyaki/workspace_venturesity/python/'
#              'project_resume/resume_samples/ChbxHEPNxs.txt', 'r').read()).publication())
