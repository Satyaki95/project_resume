import re


class Functions(object):
    def __init__(self):
        pass

    @staticmethod
    def name_split(name):
        if len(name.split(' ')) == 2:
            f_name, l_name = name.split(' ')
        else:
            f_name = name
            l_name = ''
        return f_name, l_name

    @staticmethod
    def find_work(*work):
        wrk = []
        l_yr = []
        h_yr = []
        year = list(range(1900, 2100))
        for i in work:
            for j in i:
                work_year = []
                work_year.extend(map(lambda x: re.findall(str(x), j), year))
                work_year = [i for i in work_year if i]
                work_year = [j for i in work_year for j in i]
                j = ''.join([i for i in j if not i.isdigit()])
                if not work_year:
                    l_yr.append('-')
                    h_yr.append('-')
                else:
                    work_year = sorted(list(set(work_year)))
                    l_yr.append(work_year[0])
                    h_yr.append(str(work_year[len(work_year) - 1]))
                wrk.append(j)
        return l_yr, h_yr, wrk

    @staticmethod
    def find_edu(edu):
        l_year = []
        h_year = []
        edu_list = []
        for i in edu:
            if type(i) is list:
                for j in i:
                    if type(j) is list:
                        if len(j) > 1:
                            j = [l for k in j for l in k]
                            l_year.append(j[0])
                            h_year.append(j[len(j) - 1])
                        else:
                            j = [l for k in j for l in k]
                            l_year.append(j[0])
                            h_year.append('-')
                    else:
                        edu_list.append(j)

            else:
                edu_list.append(i)
                l_year.append('-')
                h_year.append('-')
        return l_year, h_year, edu_list
