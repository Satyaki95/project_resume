import re

import nltk
from nltk.corpus import wordnet
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import PunktSentenceTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer

from functions import process_text


class FindBasics(object):
    def __init__(self, resume, path):
        self.Resume = resume
        self.text = process_text.Text(resume)
        self.lemma = WordNetLemmatizer()
        self.ps = PorterStemmer()
        self.path = path

    def main(self):
        name = self.name()
        email = self.email()
        experience = self.experience()
        summary = self.resume_summary()
        site_name, site_link, personal = self.find_social_link()
        if not name:
            name = list(email.split("@")[0])
            name = ''.join([i for i in name if not i.isdigit()])
        return name, email, experience, site_name, \
               site_link, personal, summary

    def name(self):
        count = 0
        custom_sent_tokenizer = PunktSentenceTokenizer()
        tokenize = custom_sent_tokenizer.tokenize(self.Resume)
        try:
            for i in tokenize:
                words = nltk.word_tokenize(i)
                tagged = nltk.pos_tag(words)
                named = nltk.ne_chunk(tagged, binary=True)
                nes = [' '.join(map(lambda x: x[0], ne.leaves()))
                       for ne in named if isinstance(ne, nltk.tree.Tree)]
                name = []
                if count == 0:
                    nes_list = nes[0].split(' ')
                    if len(nes_list) > 2:
                        for w in nes_list:
                            if not wordnet.synsets(w):
                                name.append(w)
                    else:
                        name = nes_list
                    count += 1
                    return ' '.join(name)
        except Exception as exception:
            print(exception)

    def email(self):
        try:
            email = re.search(r'[\w\.-]+@[\w\.-]+', self.Resume)
            return email.group(0)
        except Exception as exception:
            print(exception)

    def find_social_link(self):
        url1 = re.findall('www[s]?.(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]'
                          '|(?:%[0-9a-fA-F][0-9a-fA-F]))+', self.Resume)
        url2 = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|'
                          '(?:%[0-9a-fA-F][0-9a-fA-F]))+', self.Resume)
        url_s = list(set(url1 + url2))
        urls = []
        for i in url_s:
            for j in url_s:
                vect = TfidfVectorizer(min_df=1)
                tfidf = vect.fit_transform(
                    [i, j])
                a = (tfidf * tfidf.T).A
                if a[0][1] > 0.6:
                    urls.append([i, j])
        urls = [sorted([i, j]) for i, j in urls if i != j]
        urls = list(set([i for i, j in urls]))
        urls = list(set([x for x in url_s if x not in urls]))
        site = []
        with open(self.path + '/social_network/names.txt') as f:
            for i in f.read().split('\n'):
                site.append(i)
        links = list(set([(i, j) for i in site for j in urls if re.search(i, j)]))
        names = [i for i, j in links]
        link = [j for i, j in links]
        personal = list(set([i for i in urls if i not in link]))

        return names, link, personal

    def resume_summary(self):
        resume = [i.lower() for i in self.Resume.split('\n')]
        summary = []
        bool = False
        for i in resume:
            if not bool:
                if re.search('summary', i):
                    bool = True
            else:
                if not i:
                    if len(''.join(summary)) > 100:
                        bool = False
                elif len(''.join(summary)) > 400:
                    bool = False
                else:
                    summary.append(i)
        return '\n'.join(summary)

    def experience(self):
        try:
            result = []
            bow = self.text.process_text()
            for i in range(len(bow)):
                if self.lemma.lemmatize(self.ps.stem(bow[i])) \
                        == self.lemma.lemmatize(self.ps.stem('experience')):
                    for j in range(-5, 6):
                        word = bow[i + j]
                        num = []
                        for s in list(word):
                            if s.isdigit():
                                num.append(s)
                            elif s == '.':
                                num.append(s)
                        num = ''.join(num)
                        if num != '.':
                            num = num.split('.')
                            c = 0
                            if len(num) > 1:
                                for n in num:
                                    if wordnet.synsets(n):
                                        r = []
                                        if c == 0:
                                            r.append(wordnet.synsets(n)[0].
                                                     lemmas()[0].name() + ' year')
                                            c += 1
                                        else:
                                            r.append(wordnet.synsets(n)[0].
                                                     lemmas()[0].name() + ' months')
                                        result.append(r)
                            else:
                                for n in num:
                                    if wordnet.synsets(n):
                                        result.append(wordnet.synsets(n)[0].
                                                      lemmas()[0].name() + ' years')
                            for y in range(71):
                                nn = wordnet.synsets(str(y))[0].lemmas()[0].name()
                                if re.search(nn, word.lower()):
                                    for k in range(-3, 4):
                                        if re.search(self.ps.stem('year'),
                                                     self.ps.stem(bow[i + j + k])) \
                                                or re.search(
                                                    self.ps.stem('yr'),
                                                    self.ps.stem(bow[i + j + k])):
                                            result.append(nn)
            if len(result) != 0:
                return result
            else:
                return ['unknown']
        except Exception as exception:
            print(exception)
