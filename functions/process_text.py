import string

from nltk.corpus import stopwords


class Text(object):
    def __init__(self, text):
        self.Text = text

    def process_keywords(self):
        try:
            punctuations = list(string.punctuation)
            punctuations = ''.join([char for char in punctuations])
            no_punctuations = [char for char in self.Text if char not in punctuations]
            no_punctuations = ''.join(no_punctuations).replace('\n', ' ').strip()
            bow = [word.lower() for word in no_punctuations.split(' ') if
                   word.lower() not in stopwords.words('english')]
            bow = [x for x in bow if x]
            return bow
        except Exception as exception:
            print(exception)

    def process_text(self):
        try:
            punctuations = list(string.punctuation)
            punctuations = ''.join([char for char in punctuations if char != '.'])
            no_punctuations = [char for char in self.Text if char not in punctuations]
            no_punctuations = ''.join(no_punctuations)
            clear_msg = [word for word in no_punctuations.split() if word.lower() not in stopwords.words('english')]
            return clear_msg
        except Exception as exception:
            print(exception)
