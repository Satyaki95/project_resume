Satyaki Sanyal
Data Research Analyst Intern at Venturesity
sanyal.satyaki09@gmail.com


Summary
Satyaki is currently pursuing his under graduation in Electronics Engineering from KIIT University . A
technology enthusiast with a penchant for ideation and Innovation ,Satyaki has two important professional
student bodies , currently serving as the Assistant Coordinator of Konnexions (The IT and Web society) & The
Under Secretariat General of the IT team of KIITMUN 2016 (World's largest MUN) .


He believes in taking initiatives in diverse fields and learn through experiences. He has good experiences in
Machine learning, Data Analytics, Software Designing and development, Android development and Web
backend development.He also has a hands-on training in Embedded systems by i3 indya summer training
program in summer of 2014.
He has won the first prize in VIT's Techfest on Autonomous Robotics.
He is also proficient in programming techniques & algorithms and using computer languages like Python, C,
Embedded C, JAVA, JAVASCRIPT, HTML, CSS, PHP, MySql, Jquery and is also proficient with software
packages & applications like MS Office, AVR Studios, Arduino Studios. He has worked with frameworks like
Node.js and has knowledge of databases like parse and NoSql(MongoDB).


Satyaki is a person with an entrepreneurial mindset who is very opportunistic and optimistic about life.
He is willing to find an opportunity to work as a Data analyst or Software Developer and work for challenging
problems which make a difference.

Publications
Recruitment predictions with ID3 Decision tree
International journal of advanced engineering and research development October 22, 2016
Authors: Satyaki Sanyal, Souvik Hazra
  We have tried to solve the problem of recruitment with cognitive computing. We have used decision trees to
  predict the candidates best suited for the job and we have used random forest for better predictions .

Certifications
Data Science 101
IBM License DS0101EN December 2016
i3 indya summer training in embedded system and robotics
i3 indya


Experience


Data Research Analyst Intern at Venturesity
November 2016 - Present (2 months)

Backend Developer intern at Inceptum Technologies Pvt. Ltd
June 2016 - Present (7 months)

Student Ambassador at Mozilla
March 2016 - Present (10 months)

Software Developer at KiiT University
January 2016 - Present (1 year)
  Development and maintenance KIIT Hostel management portal is my primary task here.

Assistant Coordinator at KIIT Aeronautical Society (Apogieo)
July 2015 - Present (1 year 6 months)

Under Secretary General (The Webteam) at KIIT International MUN
June 2016 - November 2016 (6 months)
  In charge of backend development for Web, App and realtime chatting desktop software for India's largest
   MUN.

Software developer intern at Geometric Ltd.
May 2016 - June 2016 (2 months)
  Working on projects like online examination system and machine learning for geometric Limited ( A div of
  HCL)

Director of Web Development at YouthTech MegaEvent OPC Private Limited
November 2015 - March 2016 (5 months)
  My job in YouthTech is to lead the Web Team as a Senior Developer

Web Developer at KIIT Entrepreneurship cell
August 2015 - February 2016 (7 months)
  My Responsibility includes making apps and websites for startups

Project Engineer at KIIT Robotics Society
March 2015 - December 2015 (10 months)
  building a rover for university rover competition



Volunteer Experience
Mentor at IBM
September 2016 - Present



  Mentored Data Analytics and Machine Learning Session for IBM and Venturesity

Assistant Coordinator at KIIT Aeronautical Society (Apogieo)
August 2015 - Present
  I had volunteered and coordinated in our society induction and exhibition.


Education
KIIT University
Bachelor of Technology (B.Tech.), Electrical and Electronics Engineering, 2014 - 2018
Kalinga Institute Of Industrial Technology, Bhubaneshwar
Bachelor of Technology (BTech), Electrical and Electronics Engineering, 2014 - 2018
Activities and Societies: Robotics and aeronautics
Gundecha Education Academy
Indian School Certificate (ISC) Examinations, High School Certificates, 2012 - 2014
Activities and Societies: I was in Model United Nations in High school
Sri Aurobindo Institute Of Education
Indian Certificate of Secondary Education's (ICSE) examination, 1998 - 2012
Activities and Societies: In school, I was among the top twenty students. I was known for studies and
 extracurricular activities like drama, music, singing... I have won certificates for singing

Skills & Expertise
Machine Learning
Software Development
Python
PHP
Web Development
Android Application Development
Programming
Autonomous Robotics
Unmanned Aerial Vehicles
Research
Creative Writing
Video Editing
Audio Editing
embedded system
AVR Studio 4
C programming
C++
Core Java
HTML5
CSS
JavaScript
jQuery



Bootstrap
XML
AJAX
Java
MySQL
Algorithms
Electronics
C
Logistic Regression
Cascading Style Sheets (CSS)
Microsoft Office
Microsoft Word
PowerPoint
Microsoft Excel
Android Development

Languages
English
bengali
Hindi

Organizations
KIIT Mars Rover
Engineer
February 2015 to Present
KIIT Aeronautical Society (Apogieo)
Assistant Coordinator
July 2015 to Present
  currently working at a home project named, the Multimode Vehicle

Projects
The Multimode Vehicle
Members:Satyaki Sanyal
  Building a vehicle that will run, swim and fly. A home project exclusively from the college

Honors and Awards
First prize in Webathon
KIIT FEST, KIIT University
March 2016
  First prize in web development
First Prize in Robotics
VIT Vellore



September 2015
 Won first prize for Autonomous robotics in Vellore Institute of Technology's annual tech fest Gravitas2015.

Courses

Independent Coursework

python





Satyaki Sanyal
Data Research Analyst Intern at Venturesity
sanyal.satyaki09@gmail.com
+919178449492



Contact Satyaki on LinkedIn




