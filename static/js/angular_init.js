var Note = function ($scope) {
    $scope.items = [];

    $scope.add = function () {
        $scope.items.push({
            inlineChecked: false,
            question: "",
            questionPlaceholder: "foo",
            text: ""
        });
    };
};
console.log('javascript active');
var app = angular.module('myApp', [], function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<[');
    $interpolateProvider.endSymbol(']>');
});
app.controller('myCtrl', function ($scope) {
    $scope.submitForm = function () {
        console.log($scope.name);
    }
    $scope.check_f_name = function (name) {
        if (name == 'None') {
            document.getElementById("f_name_error").innerHTML = '';
            document.getElementById("f_name_ok").innerHTML = '';
        }
        else {
            if (/^[a-zA-Z]+$/.test(name)) {
                console.log(name);
                document.getElementById("f_name_error").innerHTML = '';
                document.getElementById("f_name_ok").innerHTML = '&#10003;';
            }
            else {
                document.getElementById("f_name_ok").innerHTML = '';
                document.getElementById("f_name_error").innerHTML = 'Name must not contain characters';
            }
        }
    }
    $scope.check_l_name = function (name) {
        if (name == 'None') {
            document.getElementById("l_name_error").innerHTML = '';
            document.getElementById("l_name_ok").innerHTML = '';
        }
        else {
            if (/^[a-zA-Z]+$/.test(name)) {
                console.log(name);
                document.getElementById("l_name_error").innerHTML = '';
                document.getElementById("l_name_ok").innerHTML = '&#10003;';
            }
            else {
                document.getElementById("l_name_ok").innerHTML = '';
                document.getElementById("l_name_error").innerHTML = 'Name must not contain characters';
            }
        }
    }
    $scope.check_num = function (num, iter) {
        if (num == '') {
            document.getElementById("num_error_" + iter).innerHTML = '';
            document.getElementById("num_ok_" + iter).innerHTML = '';
        }
        else {
            if (/^[0-9]+$/.test(num)) {
                console.log(num);
                document.getElementById("num_error_" + iter).innerHTML = ' ';
                document.getElementById("num_ok_" + iter).innerHTML = '&#10003;';
            }
            else {
                document.getElementById("num_ok_" + iter).innerHTML = ' ';
                document.getElementById("num_error_" + iter).innerHTML = 'Must be digits only';
            }
        }
    }
    $scope.check_code = function (num, iter) {
        if (num == '') {
            document.getElementById("code_error_" + iter).innerHTML = '';
            document.getElementById("code_ok_" + iter).innerHTML = '';
        }
        else {
            if (/^[0-9]+$/.test(num)) {
                console.log(num);
                document.getElementById("code_error_" + iter).innerHTML = '';
                document.getElementById("code_ok_" + iter).innerHTML = '&#10003;';
            }
            else {
                document.getElementById("code_ok_" + iter).innerHTML = '';
                document.getElementById("code_error_" + iter).innerHTML = 'Must be digits only';
            }
        }
    }
    $scope.check_email = function (email, iter) {
        if (email == '') {
            document.getElementById("email_error_" + iter).innerHTML = '';
            document.getElementById("email_ok_" + iter).innerHTML = '';
        }
        else {
            if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
                console.log(email);
                document.getElementById("email_error_" + iter).innerHTML = '';
                document.getElementById("email_ok_" + iter).innerHTML = '&#10003;';
            }
            else {
                document.getElementById("email_ok_" + iter).innerHTML = '';
                document.getElementById("email_error_" + iter).innerHTML = 'Not a valid email id';
            }
        }
    }
});