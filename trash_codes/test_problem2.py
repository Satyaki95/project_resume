import re
import nltk
from string import punctuation
from nltk.tokenize import PunktSentenceTokenizer
lines = open('../../resume_samples/satyaki_resume.txt', 'r', encoding='utf-8').read()
lines = ''.join([l for l in lines if l not in punctuation]).split('\n')
edu = []
year = list(range(1900, 2100))
final = []
for line in range(len(lines)):
    work = []
    list1 = list(map(lambda x: re.findall(str(x), lines[line]), year))
    list1 = [x for x in list1 if x]
    if list1:
        if line == 0:
            work.extend((lines[line], lines[line + 1], lines[line + 2]))
        elif line == 1:
            work.extend((lines[line - 1], lines[line], lines[line + 1], lines[line + 2]))

        elif line == len(lines) - 1:
            work.extend((lines[line - 2], lines[line - 1], lines[line]))
        elif line == len(lines) - 2:
            work.extend((lines[line - 2], lines[line - 1], lines[line], lines[line + 1]))
        else:
            work.extend((lines[line - 2], lines[line - 1], lines[line], lines[line + 1], lines[line + 2]))
        work = [i for i in work if i]
        final.append(work)
total = []
for line in final:
    work = []
    for l in line:
        tokenized = PunktSentenceTokenizer().tokenize(l)
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            for i, j in tagged:
                if re.search('NN', j):
                    work.append(i)
    total.append(' '.join(work))
print(total)