from __future__ import unicode_literals

import re

from nltk.stem import PorterStemmer

ps = PorterStemmer()


def resume_summary(resume):
    resume = [re.sub(r'[^\x00-\x7F]+',
                             ' ', re.sub('\s+', ' ',
                                         re.sub('[,\.\(\)\s+]', ' ', i.lower())))
                      for i in resume.split('\n')]
    summary = []
    bool = False
    for i in resume:
        if not bool:
            if re.search('project', i):
                bool = True
        else:
            if not i:
                if len(''.join(summary)) > 100:
                    bool = False
            elif len(''.join(summary)) > 400:
                bool = False
            else:
                summary.append(i)
    summary = [i for i in summary if i != ' ']
    date = list(range(1900, 2100))
    date_ret_low = []
    date_ret_high = []
    for i in summary:
        d = list(map(lambda x: re.findall(str(x), i), date))
        d = [i for i in d if i]
        d = [j for i in d for j in i]
        if d:
            date_ret_low.append(sorted(d)[0])
            date_ret_high.append(sorted(d)[len(d)-1])
        else:
            date_ret_low.append('-')
            date_ret_high.append('-')
    print(date_ret_low, date_ret_high)
    return summary, date_ret_low, date_ret_high


print(resume_summary(
    open('ruchir_resume.txt', 'r').read()))
