from pymongo import MongoClient

client = MongoClient()
db = client.keywordsDb
db2 = client.clustersDb
clusters = []
cursor = db.keywords.find({})
for c in cursor:
    clusters.append(c['expertise'])
clusters = set(clusters)
print(clusters)
for cluster in clusters:
    for sec_cluster in clusters:
        clu_1 = []
        clu_2 = []
        curse1 = db.keywords.find({'expertise': cluster})
        curse2 = db.keywords.find({'expertise': sec_cluster})
        for c1 in curse1:
            clu_1.append(c1['tag_a'])
            clu_1.append(c1['tag_b'])
        for c2 in curse2:
            clu_2.append(c2['tag_a'])
            clu_2.append(c2['tag_b'])
        clu_1 = set(clu_1)
        clu_2 = set(clu_2)
        cursor_clust = db2.clustersDb.find({'clust_a': cluster, 'clust_b': sec_cluster})
        if cursor_clust.count() == 0:
            print('new combination found... Adding to DB')
            result = db2.clustersDb.insert_one({
                'clust_a': cluster,
                'clust_b': sec_cluster,
                'similarity': len(list(set(clu_1).intersection(clu_2)))
            })
            print(len(list(set(clu_1).intersection(clu_2))), result.inserted_id)
        else:
            print('object already present. Updating...')
            result = db2.clustersDb.update_one({'clust_a': cluster, 'clust_b': sec_cluster},
                                               {"$set": {"similarity": len(list(set(clu_1).intersection(clu_2)))}})
            cursor_clust = db2.clustersDb.find({'clust_a': cluster, 'clust_b': sec_cluster})
            for cl in cursor_clust:
                    print('new similarity: ', cl['similarity'])
