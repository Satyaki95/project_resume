from time import sleep

import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient

sleep(10)
client = MongoClient()
db = client.keywordsDb
url = 'https://www.naukri.com/corporate-lawyer-jobs'

html = requests.get(url).text
soup = BeautifulSoup(html, 'html.parser')
links = soup.find_all('a', {'class': 'content'})
i = 1
for link in links:
    print(i)
    if i > 0:
        tags = []
        soup2 = BeautifulSoup(requests.get(link.get('href')).text, 'html.parser')
        for tag in soup2.find_all('font', {'class': 'hlite'}):
            tags.append(tag.text.lower().strip())
        for tag in soup2.find_all('span', {'itemprop': 'skills'}):
            tags.append(tag.text.lower().strip())
        for tag in tags:
            for j in tags:
                print(tag, '---', j)
                cursor = db.keywords.find({'tag_a': tag, 'tag_b': j, 'expertise': 'legal'})
                if cursor.count() == 0:
                    print('New combination found. Adding...')
                    if tag == j:
                        c = 0
                    else:
                        c = float(2 ** 10)
                    result = db.keywords.insert_one({
                        'tag_a': tag,
                        'tag_b': j,
                        'eu_dist': c / 2.0,
                        'expertise': 'legal'
                    })
                    print('Successfully added to database with id = ', result.inserted_id)
                else:
                    print('object already present. Updating...')
                    for counter in cursor:
                        inc_count = counter['eu_dist']
                        if inc_count != 0:
                            inc_count /= 2.0
                        print('new count = ', inc_count)
                    cursor = db.keywords.find({'tag_a': tag, 'tag_b': j, 'expertise': 'legal'})
                    result = db.keywords.update_one({'tag_a': tag, 'tag_b': j, 'expertise': 'legal'},
                                                    {"$set": {'eu_dist': inc_count}})
                    for counter in cursor:
                        print('Updated Successfully\n', counter['expertise'], '\n', counter['tag_a'], '--',
                              counter['tag_b'], '---', counter['eu_dist'])
            sleep(10)
    i += 1
    # computer_science
    # human_resource
    # finance
    # management - make strong
    # marketing - make strong (including sales)
    # legal - make strong
