import collections
import json
import math
import os
import re
import string

import nltk
import numpy as np
import pandas as pd
import phonenumbers
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import PunktSentenceTokenizer
from phonenumbers import geocoder
from pymongo import MongoClient
from sklearn.feature_extraction.text import TfidfVectorizer

from functions.find_name import FindName


lemmatizer = WordNetLemmatizer()
ps = PorterStemmer()


def main():
    # try:
    client = MongoClient()
    db = client.keywordsDb
    db2 = client.clustersDb
    for f in os.listdir('../resume_samples'):
        print(f)
        json_dumper = {'path': [], 'resume type': [],
                       'name': [], 'email': [], 'phone': [],
                       'location': [], 'years_of_experience': [],
                       'skills': [], 'education': [], 'years_of_study': []}
        json_dumper['path'].append(f)
        resume = open('../resume_samples/' + f, 'r', encoding="utf8", errors='ignore').read()
        name = FindName(resume)
        print(name.find())
        # email = check_email(resume)
        # if not name:
        #     name = list(email.split("@")[0])
        #     name = ''.join([i for i in name if not i.isdigit()])
        # json_dumper['name'].append(name)
        # json_dumper['email'].append(email)
        # num = extract_phone_numbers(resume, str('../resume_samples/' + f))
        # json_dumper['phone'].append(list(set(num)))
        # feature, skills = find_cluster(resume, db, db2)
        # json_dumper['resume type'].append(list(feature))
        # json_dumper['skills'].append(list(skills))
        # print(feature, skills)
        # study_exp, edu = find_education(resume)
        # json_dumper['education'].append(edu)
        # json_dumper['years_of_study'].append(study_exp)
        # try:
        #     number = str("+" + str(num[0]))
        #     loc = find_location(number)
        #     if loc == '':
        #         loc = 'unknown'
        # except:
        #     loc = 'unknown'
        # json_dumper['location'].append(loc)
        # exp = find_experience(resume)
        # json_dumper['years_of_experience'].append(exp)
        # print("name :", name, " || email :", email, " || number:", num, " || location :", loc,
        #       " || \nyears of experience :", exp)
        # print(collections.OrderedDict(sorted(json_dumper.items())))
        # with open('../json_resume/' + f.replace('.txt', '') + '.json', 'w') as fp:
        #     json.dump(json_dumper, fp, indent=4, sort_keys=True)
        #     print('resume saved')
        # print('\n\n')
        # except Exception as e:
        #     print(e)


# def find_name(resume):
#     count = 0
#     custom_sent_tokenizer = PunktSentenceTokenizer()
#     tokenized = custom_sent_tokenizer.tokenize(resume)
#     try:
#         for i in tokenized:
#             words = nltk.word_tokenize(i)
#             tagged = nltk.pos_tag(words)
#             namedEnt = nltk.ne_chunk(tagged, binary=True)
#             nes = [' '.join(map(lambda x: x[0], ne.leaves())) for ne in namedEnt if isinstance(ne, nltk.tree.Tree)]
#             name = []
#             if count == 0:
#                 nes_list = nes[0].split(' ')
#                 if len(nes_list) > 2:
#                     for w in nes_list:
#                         if not wordnet.synsets(w):
#                             name.append(w)
#                 else:
#                     name = nes_list
#                 count += 1
#                 return ' '.join(name)
#     except Exception as e:
#         print(e)


# def check_email(document):
#     email = re.search(r'[\w\.-]+@[\w\.-]+', document)
#     return email.group(0)


# def text_process(crap):
#     punc = list(string.punctuation)
#     punc = ''.join([char for char in punc if char != '.'])
#     nopunc = [char for char in crap if char not in punc]
#     nopunc = ''.join(nopunc)
#     clear_msg = [word for word in nopunc.split() if word.lower() not in stopwords.words('english')]
#     return clear_msg


# def text_process_keywords(resume):
#     punc = list(string.punctuation)
#     punc = ''.join([char for char in punc])
#     nopunc = [char for char in resume if char not in punc]
#     nopunc = ''.join(nopunc).replace('\n', ' ').strip()
#     bow = [word.lower() for word in nopunc.split(' ') if word.lower() not in stopwords.words('english')]
#     bow = [x for x in bow if x]
#     return bow


# def find_skills(bow, db):
#     skills = []
#     for i in bow:
#         cursor = db.keywords.find({'tag_a': i})
#         if cursor.count() > 0:
#             skills.append(i)
#     list_1 = set(skills)
#     return list_1, skills


# def find_stream(list_1, db):
#     k = 0
#     df = pd.DataFrame(index=list_1, columns=list_1)
#     for i in list_1:
#         for j in list_1:
#             k += 1
#             cursor = db.keywords.find({"tag_a": {'$regex': i}, "tag_b": {'$regex': j}})
#             if cursor.count() > 0:
#                 for l in cursor:
#                     if l == '':
#                         pass
#                     df[i][j] = l['eu_dist']
#             else:
#                 df[i][j] = 1024
#     l = []
#     l.append([""] + list(df.columns.values))
#     for x, item in enumerate(df.index.values):
#         l.append([item] + list(df.values[x]))
#     list_2 = [i for i in df[df > .01].min()]
#     k = 0
#     json = {}
#     for i in list_2:
#         json[i] = []
#
#     for i in df:
#         for j in df.columns.values:
#             if list_2[k] == df[i][j]:
#                 json[df[i][j]].append([i, j])
#         k += 1
#     p = math.ceil(0.2 * len(json))
#     od = collections.OrderedDict(sorted(json.items()))
#     kk = 0
#     exp = {}
#     for key in od:
#         if kk < p:
#             for i in od[key]:
#                 cursor = db.keywords.find({'tag_a': {'$regex': i[0]}, 'tag_b': {'$regex': i[1]}})
#                 for c in cursor:
#                     if c['eu_dist'] not in exp:
#                         exp.update({c['eu_dist']: []})
#                     exp[c['eu_dist']].append(c['expertise'])
#         kk += 1
#     kk = 0
#     od2 = collections.OrderedDict(sorted(exp.items()))
#     return_element = []
#     for key in od2:
#         if kk < 1:
#             for i in set(od2[key]):
#                 return_element.append(i)
#             kk += 1
#     return return_element


# def find_cluster(resume, db, db2):
#     print('Please wait as we process your resume...')
#     list_1, skills = find_skills(text_process_keywords(resume), db)
#     dict = sorted(collections.Counter(skills).items(), key=lambda x: x[1])
#     num_list = []
#     for i, j in dict:
#         num_list.append(j)
#     num_list = set(num_list)
#     num_list = np.array(list(map(int, num_list)))
#     num_list = num_list[int(np.median(num_list)):]
#     if len(num_list) >= 4:
#         num_list = [x for x in num_list if x > int(np.median(num_list))]
#     list_f = [i for i, j in dict if j in num_list]
#     first_set = find_stream(list_f, db)
#     second_set = find_stream(list_1, db)
#     cluster_select = {}
#     for cl in first_set:
#         for s_cl in second_set:
#             cursor_clust = db2.clustersDb.find({'clust_a': cl, 'clust_b': s_cl})
#             for c in cursor_clust:
#                 if int(c['similarity']) not in cluster_select:
#                     cluster_select.update({int(c['similarity']): []})
#                     cluster_select[int(c['similarity'])].append([cl, s_cl])
#
#     cluster_select = collections.OrderedDict(sorted(cluster_select.items()))
#     probable = []
#     clus_count = 0
#     for key in cluster_select:
#         if clus_count == 0:
#             for i, j in cluster_select[key]:
#                 if i != j:
#                     clus_count += 1
#                 probable.append(i)
#                 probable.append(j)
#     return set(probable), list_1


# def find_experience(document):
#     result = []
#     bow = text_process(document)
#     for i in range(len(bow)):
#         if lemmatizer.lemmatize(ps.stem(bow[i])) == lemmatizer.lemmatize(ps.stem('experience')):
#             for j in range(-5, 6):
#                 word = bow[i + j]
#                 num = []
#                 for s in list(word):
#                     if s.isdigit():
#                         num.append(s)
#                     elif s == '.':
#                         num.append(s)
#                 num = ''.join(num)
#                 if num != '.':
#                     num = num.split('.')
#                     c = 0
#                     if len(num) > 1:
#                         for n in num:
#                             if wordnet.synsets(n):
#                                 r = []
#                                 if c == 0:
#                                     r.append(wordnet.synsets(n)[0].lemmas()[0].name() + ' year')
#                                     c += 1
#                                 else:
#                                     r.append(wordnet.synsets(n)[0].lemmas()[0].name() + ' months')
#                                 result.append(r)
#                     else:
#                         for n in num:
#                             if wordnet.synsets(n) != []:
#                                 result.append(wordnet.synsets(n)[0].lemmas()[0].name() + ' years')
#                     for y in range(71):
#                         nn = wordnet.synsets(str(y))[0].lemmas()[0].name()
#                         if re.search(nn, word.lower()):
#                             for k in range(-3, 4):
#                                 if re.search(ps.stem('year'), ps.stem(bow[i + j + k])) or re.search(ps.stem('yr'),
#                                                                                                     ps.stem(bow[
#                                                                                                                                 i + j + k])):
#                                     result.append(nn)
#
#         else:
#             pass
#     if len(result) != 0:
#         return result
#     else:
#         return ['unknown']


# def extract_phone_numbers(string, path):
#     r = re.compile(r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')
#     phone_numbers = r.findall(string)
#     extracted = [re.sub(r'\D', '', number) for number in phone_numbers if len(number) > 8]
#     with open(path) as f:
#         content = f.readlines()
#     number = []
#     for i in content:
#         num = []
#         for j in i:
#             if j.isdigit():
#                 num.append(j)
#         for k in extracted:
#             if str(k) in str(''.join(num)):
#                 number.append(''.join(num))
#     formatted_num = []
#     for k in extracted:
#         for i in number:
#             N = str(k[0]) + str(k[1]) + str(k[2])
#             if k[0] == i[0] and k[1] == i[1] and k[2] == i[2]:
#                 formatted_num.append(i)
#             elif N in i:
#                 if i[i.index(N[0])] == N[0] and i[i.index(N[0]) + 1] == N[1] and i[i.index(N[0]) + 2] == N[2]:
#                     formatted_num.append(i[i.index(N[0]):])
#     if len(formatted_num) != 0:
#         return formatted_num
#     else:
#         return number


# def find_location(num):
#     ch_number = phonenumbers.parse(num, "CH")
#     return repr(geocoder.description_for_number(ch_number, "en"))


def find_education(lines):
    edu = []
    year = list(range(1900, 2100))
    for line in lines.split('\n'):
        l = ' '.join([l.lower() for l in line.split()])
        if 'education' in l or 'university' in l or 'college' in l or 'school' in l or 'institute' in l:
            edu.append(line)
    yr = []
    calc_yr = []
    for i in edu:
        sent = []
        clear = ' '.join([word for word in i.split() if word.lower() not in stopwords.words('english')])
        list2 = list(map(lambda x: re.findall(str(x), clear), year))
        list2 = [x for x in list2 if x != []]
        list2 = [k for k in list2]
        clear = ''.join([k for k in clear if k not in string.punctuation and not k.isdigit()]).strip()
        custom_sent_tokenizer = PunktSentenceTokenizer()
        tokenized = custom_sent_tokenizer.tokenize(clear)
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            for k, j in tagged:
                if 'NNP' in j or 'JJ' in j:
                    sent.append(k)
        clear2 = ' '.join(sent).lower()
        clear = clear.lower()
        vect = TfidfVectorizer(min_df=1)
        tfidf = vect.fit_transform(
            [clear, clear2])
        a = (tfidf * tfidf.T).A
        if a[0][1] > 0.2:
            if len(clear) > len(clear2):
                if not list2:
                    yr.append(clear)
                else:
                    yr.append([clear, list2])
            else:
                if not list2:
                    yr.append(clear2)
                else:
                    yr.append([clear2, list2])
        else:
            if not list2:
                yr.append(clear)
                yr.append(clear2)
            else:
                yr.append([clear, list2])
                yr.append([clear2, list2])
        if list2:
            for j in list2:
                for k in j:
                    calc_yr.append(k)
    if calc_yr:
        calc = int(max(calc_yr)) - int(min(calc_yr))
    else:
        calc = ''
    l_crap = ['education', 'university', 'college', 'school', 'institute']
    yr = [y for y in yr if y not in l_crap]
    yr = [y for y in yr if y]
    return calc, yr


if __name__ == '__main__':
    main()
# /home/satyaki/workspace/python/resume-parser/resume_samples/akasha_resume.txt
