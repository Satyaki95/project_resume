import os
import random
import string

from flask import Flask

# from werkzeug.utils import secure_filename
#
app = Flask(__name__)
UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/uploads/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
#
#
# @app.route('/')
# def index():
#     return redirect(url_for('hello'))
#
#
# @app.route('/hello/')
# @app.route('/hello/<name>')
# def hello(name=None):
#     return render_template('index.html', name=name)
#
#
# @app.route('/upload/', methods=['GET', 'POST'])
# def upload_file():
#     if request.method == 'POST':
#         file = request.files['file[]']
#         if file:
#             filename = secure_filename(file.filename)
#             file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
#             return hello()
#     return render_template('upload.html')
#
#
# if __name__ == '__main__':
#     app.run(debug=True)
print(os.listdir(app.config['UPLOAD_FOLDER'], ))
print(''.join(
    random.SystemRandom().choice(string.ascii_uppercase + string.digits + string.ascii_lowercase + string.ascii_letters)
    for _ in range(60)))
